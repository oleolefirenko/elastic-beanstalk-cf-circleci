#!/usr/bin/env bash

usage="Usage: $(basename "$0") region stack-name [extra-opts]

where:
  region     - the AWS region
  stack-name - the stack name
  extra-opts - extra options passed directly to AWS create-stack or update-stack
"
region="eu-west-1"
tmpPath="file://$PWD/template.yaml"

AWS_ACCESS_KEY_ID=$(cat ~/.aws/credentials | grep aws_access_key_id | cut -d'=' -f2 | sed 's/^[ \t]*//;s/[ \t]*$//')
AWS_SECRET_ACCESS_KEY=$(cat ~/.aws/credentials | grep aws_secret_access_key | cut -d'=' -f2 | sed 's/^[ \t]*//;s/[ \t]*$//')

datetime="$(date +%s%N | cut -b1-13)-$(date -u +"%Y-%m-%dT%H:%M:%SZ")"

parameters="ParameterKey=awsAccessKey,ParameterValue=$AWS_ACCESS_KEY_ID \
ParameterKey=awsAccessKey,ParameterValue=$AWS_SECRET_ACCESS_KEY \
ParameterKey=s3Bucket,ParameterValue=$S3_BUCKET \
ParameterKey=s3Key,ParameterValue=$datetime/$ARTIFACTS_NAME.zip \
ParameterKey=instanceType,ParameterValue=$INSTANCE_TYPE
"

if [ "$1" == "-h" ] || [ "$1" == "--help" ] || [ "$1" == "help" ] || [ "$1" == "usage" ] ; then
  echo "$usage"
  exit
fi

shopt -s failglob
set -eu -o pipefail

echo "Uploading artifacts to s3 ..."
aws s3 sync $CIRCLE_ARTIFACTS s3://$S3_BUCKET/$datetime

echo "Checking if stack exists ..."

if ! aws cloudformation describe-stacks --region $region --stack-name $1 ; then

  echo -e "\nStack does not exist, creating ..."
  aws cloudformation create-stack \
    --region $region \
    --stack-name $1 \
    --template-body $tmpPath \
    --parameters $parameters \
    ${@:2}

  echo "Waiting for stack to be created ..."
  aws cloudformation wait stack-create-complete \
    --region $region \
    --stack-name $1 \

else

  echo -e "\nStack exists, updating ..."
  aws cloudformation update-stack \
    --region $region \
    --stack-name $1 \
    --template-body $tmpPath \
    --parameters $parameters\
    ${@:2}

  echo "Waiting for stack update to complete ..."
  aws cloudformation wait stack-update-complete \
    --region $region \
    --stack-name $1 \

fi

echo "Stack create/update completed successfully!"